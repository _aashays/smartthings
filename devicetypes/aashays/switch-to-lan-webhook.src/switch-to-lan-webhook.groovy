/**
 *  switch-to-lan-webhook
 *
 *  Copyright 2020 Aashay Shringarpure
 *
 *  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 *  in compliance with the License. You may obtain a copy of the License at:
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software distributed under the License is distributed
 *  on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License
 *  for the specific language governing permissions and limitations under the License.
 */
metadata {
	definition (name: "Switch to Lan Webhook", namespace: "aashays", author: "Aashay Shringarpure", cstHandler: true) {
		capability "Switch"
	}

	preferences {
		section(install: true) {
			input "webhookIP", "string", title:"Webhook server IP", required: true
			input "webhookPort", "string", title:"Webhook server Port", required: true
			input "webhookId", "string", title:"ID of the webhook to call", required: true
            input "overrideOnState", "string", title:"JSON state value for On", required: false
            input "overrideOffState", "string", title:"JSON state value for Off", required: false
		}
	}

	tiles {
		standardTile("button", "device.switch", width: 2, height: 2, canChangeIcon: true) {
			state "offReady", label: 'Off', action: "switch.on", icon: "st.switches.switch.off", backgroundColor: "#bfbfbf", nextState: "onReady"
			state "onReady", label: 'On', action: "switch.off", icon: "st.switches.switch.on", backgroundColor: "#79b821", nextState: "offReady"
			state "off", label: 'Off', action: "switch.on", icon: "st.switches.switch.off", backgroundColor: "#bfbfbf"
			state "on", label: 'On', action: "switch.off", icon: "st.switches.switch.on", backgroundColor: "#79b821"
		}
		standardTile("buttonOn", "device.switchOn", width: 1, height: 1) {
			state "on", label: 'On', action: "switch.on", icon: "st.switches.switch.on", backgroundColor: "#79b821"
		}
		standardTile("buttonOff", "device.switchOff", width: 1, height: 1) {
			state "off", label: 'Off', action: "switch.off", icon: "st.switches.switch.off", backgroundColor: "#bfbfbf"
		}
		main "button"
		details "button","buttonOn","buttonOff"
	}
}

def webhookCallback(physicalgraph.device.HubResponse hubResponse) {
	log.debug "hubResponse status '${hubResponse.status.toString()}'"
	log.debug "hubResponse headers '${hubResponse.headers.toString()}'"
	if (hubResponse.json) log.debug "hubResponse json '${hubResponse.json.toString()}'"
	if (hubResponse.body) log.debug "hubResponse body '${hubResponse.body.toString()}'"

}

private sendWebhook(boolean on) {
	def onPayload = settings.overrideOnState ? settings.overrideOnState : 'on'
	def offPayload = settings.overrideOffState ? settings.overrideOffState : 'off'
	def hubAction = new physicalgraph.device.HubAction([
		method: 'PUT',
		path: "/hooks/${settings.webhookId}",
		headers: [
			'HOST': "${settings.webhookIP}:${settings.webhookPort}",
			'Content-type': 'application/json',],
		query: [],
		body: [
			'state': on ? onPayload : offPayload,]],
			null,
		[callback: webhookCallback]
	)
	log.debug "hubAction '${hubAction.toString()}'"
	try {
		sendHubCommand(hubAction)
	} catch (Exception e) {
		log.error "sendHubCommand failed with ${e}"
	}
}

// handle commands
def on() {
	log.debug "Executing 'on'"
	sendEvent(name: "switch", value: "on")
	return sendWebhook(true)
}

def off() {
	log.debug "Executing 'off'"
	sendEvent(name: "switch", value: "off")
	return sendWebhook(false)
}
