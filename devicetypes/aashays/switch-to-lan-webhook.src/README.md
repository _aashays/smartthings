Simple "Switch" device that can call a local webhook. Works with [adnanh/webhook](https://github.com/adnanh/webhook).

Sample configuration:

```
  {
    "id": "tv-night-mode",
    "trigger-rule":
    {
      "match":
      {
        "type": "regex",
        "value": "(on|off)",
        "parameter":
        {
          "source": "payload",
          "name": "state"
        }
      }
    },
    "execute-command": "/etc/webhook/tv_night_mode.sh",
    "pass-arguments-to-command": [
    {
      "source": "payload",
      "name": "state"
    }],
    "command-working-directory": "/etc/webhook",
    "include-command-output-in-response": false
  }
```
